# 📄 MicroLogger
A "micro" logger that follows the syslog standard, written in [Go](https://go.dev/).

## Demo
[![asciicast](https://asciinema.org/a/r6jgZRdvIQD0MXENx1wW9Ghsf.svg)](https://asciinema.org/a/r6jgZRdvIQD0MXENx1wW9Ghsf)

## Installation
You can install MicroLogger in your project using `go get gitlab.com/infinitisoftware/tesseract/MicroLogger`

## Usage
You can find more information, along with detailed usage examples at [our wiki](https://gitlab.com/infinitisoftware/tesseract/MicroLogger/-/wikis/home)

## Support
There are two ways you can receive support for MicroLogger, through our [issue tracker](https://gitlab.com/infinitisoftware/tesseract/MicroLogger/-/issues), or by composing a message to our [service desk](mailto:contact-project+infinitisoftware-tesseract-micrologger-32877181-issue-@incoming.gitlab.com?subject=Please%20specify%20an%20issue%20title&body=Please%20describe%20your%20issue%20in%20detail%20here.) email.

## Authors and Acknowledgment
This project was developed by the Tesseract team from [Infiniti Software](https://gitlab.com/infinitisoftware)

## License
This project is licensed under the [BSD 3-Clause "New" or "Revised" License](https://tldrlegal.com/license/bsd-3-clause-license-(revised)) [(fulltext)](LICENSE)
